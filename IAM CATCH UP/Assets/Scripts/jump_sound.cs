using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jump_sound : MonoBehaviour
{
    [FMODUnity.EventRef] public string jump_event;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void jump()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(jump_event, gameObject);
    }
}
