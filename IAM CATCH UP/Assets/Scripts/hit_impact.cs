using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using Invector.vMelee;
public class hit_impact : MonoBehaviour 
{

    [FMODUnity.EventRef] public string Hitevent; 
    public vThirdPersonInput PlayerInput;
    void Start()

    {

        PlayerInput = GetComponent<vThirdPersonInput>();

    }
    public void PlayerHit()

    {
        FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(Hitevent);
        eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
        eventInstance.start();
        eventInstance.release();
    }
}