using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class landlow_sound : MonoBehaviour
{
    [FMODUnity.EventRef] public string landlow_event;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void land_low()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(landlow_event, gameObject);
    }
}
