using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class low_health : MonoBehaviour
{
    [FMODUnity.EventRef] public string snapevent; //фильтр 
    private FMOD.Studio.EventInstance snapinstance;

    [FMODUnity.EventRef] public string heartbeatevent; //сердцебиение
    private FMOD.Studio.EventInstance heartbeatinstance;

    public vThirdPersonController control;
    public bool SnapShotisPlaying = false;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(control.currentHealth);

        if (!SnapShotisPlaying)


        {

            if (control.currentHealth <= 30)
            {

                snapinstance = FMODUnity.RuntimeManager.CreateInstance(snapevent);
                snapinstance.start();

                heartbeatinstance = FMODUnity.RuntimeManager.CreateInstance(heartbeatevent);
                heartbeatinstance.start();

                SnapShotisPlaying = true;

            }





        }

        else
        {
            if (control.currentHealth > 30)
            {
                snapinstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);

                heartbeatinstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);

                SnapShotisPlaying = false;


            }
        }

    }
}




    
