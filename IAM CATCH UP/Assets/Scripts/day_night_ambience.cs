using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class day_night_ambience : MonoBehaviour
{
    [FMODUnity.EventRef] public string amb_village;
    private FMOD.Studio.EventInstance amb_instace;
    public DayNightController dnc;
    //public GameObject daynight;
   
   

    void Start()
    {
        amb_instace = FMODUnity.RuntimeManager.CreateInstance(amb_village);
        amb_instace.start();
        //dnc = daynight.GetComponent<DayNightController>();
    }

   
    void Update()
    {
        
        // windinstace.setParameterByName("daynight", dnc.currentTime);
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("day_night", dnc.currentTime); // передает глобальное значние параметр
        amb_instace.setParameterByName("day_night", dnc.currentTime);
        //Debug.Log(dnc.currentTime);
        
    }
}
