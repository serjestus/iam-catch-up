using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class landhigh_sound : MonoBehaviour
{
    [FMODUnity.EventRef] public string landhigh_event;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void land_high()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(landhigh_event, gameObject);
    }
}
