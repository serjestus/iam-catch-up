using System.Collections;
using System.Collections.Generic;
using FMOD.Studio;
using UnityEngine;

public class VCAVolume : MonoBehaviour
{
    private UnityEngine.UI.Slider ourSlider;
    private FMOD.Studio.VCA ourVCA;
    public string VCAName;
    
    
    // Start is called before the first frame update
    void Start()
    {
        ourSlider = gameObject.GetComponent<UnityEngine.UI.Slider>();
        ourVCA = FMODUnity.RuntimeManager.GetVCA("vca:/" + VCAName);
    }

 public void VCAVolumeChange()
    {
        ourVCA.setVolume(ourSlider.value);
    }
}
