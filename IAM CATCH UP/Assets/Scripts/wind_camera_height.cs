using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wind_camera_height : MonoBehaviour
{
    public GameObject camera;
    [FMODUnity.EventRef] public string windevent;
    private FMOD.Studio.EventInstance windinstace;
    public DayNightController dnc;
   // public GameObject daynight;
   
   

    void Start()
    {
        windinstace = FMODUnity.RuntimeManager.CreateInstance(windevent);
        windinstace.start();
        //dnc = daynight.GetComponent<DayNightController>();
    }

   
    void Update()
    {
        windinstace.setParameterByName("camera_height", camera.transform.position.y);
       // windinstace.setParameterByName("daynight", dnc.currentTime);
       FMODUnity.RuntimeManager.StudioSystem.setParameterByName("daynight", dnc.currentTime); // передает глобальное значние параметр
       Debug.Log(dnc.currentTime);
        
    }
}
