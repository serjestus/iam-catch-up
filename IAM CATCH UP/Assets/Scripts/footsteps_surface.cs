using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Invector.vCharacterController;

public class footsteps_surface : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string footsteps_event;

    public vThirdPersonInput tpInput;
    public LayerMask sloy;
    public float tipPoverhnosti;
    
    // Start is called before the first frame update
    void Start()
    {
        tpInput = GetComponent<vThirdPersonInput>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void footstep()
    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {
            ProverkaPoverhnosti();
            FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(footsteps_event);
            eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));

            eventInstance.setParameterByName("RunToSprint", tpInput.cc.inputMagnitude);

            eventInstance.setParameterByName("surface_type", tipPoverhnosti);
            eventInstance.start();
            eventInstance.release();
        }
        
    }

    void ProverkaPoverhnosti()
    {
        RaycastHit luch;
        if (Physics.Raycast(transform.position, Vector3.down, out luch, 0.3f, sloy))
        {
            Debug.Log(luch.collider.tag);
            if (luch.collider.CompareTag("vSnow")) tipPoverhnosti = 0;
            else if (luch.collider.CompareTag("vWood+Snow")) tipPoverhnosti = 1;
            else if (luch.collider.CompareTag("vWood")) tipPoverhnosti = 2;
            else if (luch.collider.CompareTag("vWater")) tipPoverhnosti = 4;
            else if (luch.collider.CompareTag("vRock")) tipPoverhnosti = 3;
            else tipPoverhnosti = 0;
        }
    }
}
