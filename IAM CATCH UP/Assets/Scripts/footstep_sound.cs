using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class footstep_sound : MonoBehaviour


{
    [FMODUnity.EventRef] public string footstep_event;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void footstep()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(footstep_event, gameObject);
    }
}
